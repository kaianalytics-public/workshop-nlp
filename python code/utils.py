# coding: utf-8

import pickle
#import os.path as path
import os
from sklearn.model_selection import train_test_split



def save_model(folder, file_name, model):
    """
    Given a folder name, file name, and model object, save the model on 
    the disk.
    
    Parameters:
    folder (str)    -- folder to the folder
    file_name (str) -- name of the saved model
    model (object)  -- model object to be saved
    
    Returns:
    None
    """
    file = os.path.join(folder, file_name)
      
    with open(file, "wb") as f:
        pickle.dump(model, f)
    


def load_model(folder, file_name):
    """
    Given a folder name and file name, load the model from the disk.
    
    Parameters:
    folder (str)    -- folder to the folder
    file_name (str) -- name of the saved model
    
    Returns:
    model (object)  -- model loaded from the disk
    """

    try:
        with open(os.path.join(folder, file_name), "rb") as f:
            model = pickle.load(f)
        return model

    except Exception as e:
        print(e, "utils.py")


def split_data(X, y, test_size=0.2):
    """
    Split data into train, validation, test sets.
    
    Parmeters:
    X (dataframe, list) -- features of the dataset
    y (dataframe, list) -- target of the dataset
    test_size (float)   -- split ratio for test dataset 
                           (e.g., 0.2 means 20% goes to the test set)
    
    Returns:
    X_train (dataframe, list) -- features of the training set
    y_train (dataframe, list) -- targets of the training set
    X_valid (dataframe, list) -- features of the validation set
    y_valid (dataframe, list) -- targets of the validation set
    X_test  (dataframe, list) -- features of the test set
    y_test  (dataframe, list) -- targets of the test set   
    """
    X_trainvalid, X_test, y_trainvalid, y_test = train_test_split(X, y, 
                                                                  test_size=test_size, 
                                                                  random_state=0)
    X_train, X_valid, y_train, y_valid = train_test_split(X_trainvalid, 
                                                          y_trainvalid, 
                                                          test_size=test_size, 
                                                          random_state=0)

    if isinstance(X, list):
        print("Train data sample size:: {}".format(len(y_train)))
        print("Validation data sample size: {}".format(len(y_valid)))
        print("Test data sample size: {}".format(len(y_test)))
    else:
        print("Train data sample size:", X_train.shape[0])
        print("Validation data sample size:", X_valid.shape[0])
        print("Test data sample size:", X_test.shape[0])    
    return X_train, y_train, X_valid, y_valid, X_test, y_test
