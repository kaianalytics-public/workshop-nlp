# coding: utf-8

# utilities
import time
import numpy as np
import pandas as pd
import os.path as path

# custom script
import utils

# models
#import gensim
from gensim.models import Phrases, LdaModel
from gensim.models.phrases import Phraser
from gensim.corpora import Dictionary

# visualization
import pyLDAvis.gensim
import matplotlib.pyplot as plt
# from wordcloud import WordCloud

# metrics
from contextualized_topic_models.evaluation.measures \
   import TopicDiversity, CoherenceWordEmbeddings, InvertedRBO


class GensimDataLoader(object):

    def __init__(self, work_folder, file_path, field_name):
        self.work_folder = work_folder
        self.file_path = file_path
        self.field_name = field_name
        self.n = None  # used for BoW assertion
        self.corpus_list = None  # list of tokenized docs (either comments or sentences)
        self.corpus_bigrams = None
        self.dictionary = None
        self.corpus_bow = None

    def transform_data(self):

        try:
            corpus_df = self._set_dataframe()
            self.corpus_list = [str(doc).split() for doc in corpus_df[self.field_name].tolist()]
            self.n = len(self.corpus_list)
            print("Ready to create a Dictionary.")

        except Exception as e:
            print("Failed to complete preparations for Dictionary creation.", e)

        try:
            print("Creating a Dictionary...")
            corpus_bigrams, dictionary = self.create_dictionary()
            self.corpus_bigrams = corpus_bigrams
            self.dictionary = dictionary
            print("Dictionary was successfully created.")
            print("The first 5 vocabs:", list(self.dictionary.values())[:5])
            print("The size of vocab:", len(self.dictionary))

        except Exception as e:
            print(f"Failed in creating Dictionary. {e}")

        try:
            print("Creating BoW representation...")
            self.corpus_bow = self.create_corpus_bow()
            print("Bow representation was successfully created.")

        except Exception as e:
            print(f"Failed in creating BoW representation. {e}")

    def _set_dataframe(self):
        corpus_df = pd.read_csv(self.file_path, encoding='utf8',
                                index_col='doc_id')

        return corpus_df

    def create_dictionary(self):

        bigram_phrases = Phrases(self.corpus_list, delimiter=' ')
        bigram_phraser = Phraser(bigram_phrases)
        corpus_bigrams = [bigram_phraser[doc] for doc in self.corpus_list]
        dictionary = Dictionary(corpus_bigrams)

        try:
            assert (len(dictionary) > 0)
            objects = [
                ('bigram_phrases', bigram_phrases),
                ('bigram_phraser', bigram_phraser),
                ('corpus_bigrams', corpus_bigrams),
                ('dictionary', dictionary)
            ]
            for filename, obj in objects:
                utils.save_model(self.work_folder, filename, obj)

            return corpus_bigrams, dictionary

        except Exception as e:
            print(e)

    def create_corpus_bow(self):

        # Create a list of dense matrices
        bow = [self.dictionary.doc2bow(doc) for doc in self.corpus_bigrams]

        try:
            assert (len(bow) == self.n)
            utils.save_model(self.work_folder, "corpus_bow", bow)
            return bow

        except Exception as e:
            print(e)


# --------- Training phase ---------

class GensimTrainer(object):

    def __init__(self, work_folder, file_path, field_name):
        self.work_folder = work_folder
        self.file_path = file_path
        self.field_name = field_name
        #self.n = None  # used for BoW assertion
        self.corpus_list = None  # list of tokenized docs (either comments or sentences)
        self.dictionary = None
        self.corpus_bow = None
        self.model = None
        self.k = None  # better to be set by train method, not by init as it allows training with different k's
        self.topic_dict = None

    def pipeline(self, n_topics=100, passes=1, iterations=100):

        try:
            print("Preparing Gensim Dataset...")
            loader = GensimDataLoader(self.work_folder, self.file_path, self.field_name)
            loader.transform_data()
            #corpus_df = self._set_dataframe()
            #self.n = loader.n
            self.corpus_list = loader.corpus_list
            #            self.corpus_bigrams = loader.corpus_bigrams
            self.dictionary = loader.dictionary
            self.corpus_bow = loader.corpus_bow
            print("Gensim Dataset was successfully created.")

        except Exception as e:
            print("Failed in creating Gensim Dataset.", e)

        try:
            print("Training Gensim LDA model...")
            self.model = self.train(n_topics, passes, iterations)
            print(f"Gensim LDA model was successfully created.")

        except Exception as e:
            print("Failed in training Gensim LDA model.", e)

    def train(self, n_topics=100, passes=1, iterations=100):  # Gensim's default is 100
        # https://groups.google.com/forum/#!topic/gensim/z0wG3cojywM

        self.k = n_topics

        try:
            start = time.time()
            lda = LdaModel(
                corpus=self.corpus_bow,  # matrix of shape (num_terms, num_documents)
                id2word=self.dictionary,
                num_topics=n_topics,
                passes=passes,  # increase for a small corpus
                iterations=iterations,  # E-step for each document; # default 50
                random_state=1,
                alpha='asymmetric',
                minimum_probability=0,  # important!!
            )

            # Save the last model
            utils.save_model(self.work_folder, "gensim_model", lda)

            stop = time.time()
            time_elapsed = stop - start
            print(f"Total time elapsed: {time_elapsed:.2f}s, {time_elapsed / 60:.2f} min")
            return lda

        except Exception as e:
            print(e)


    def evaluate(self):

        self.topic_dict = self.get_topic_dict(n_keywords=25)

        scores = {'topic_diversity': [],
                  'inverted_rbo': [],
                  # 'coherence_we': [],
                  }

        top_25_keywords = list(self.topic_dict.values())[:25]
        top_10_keywords = list(self.topic_dict.values())[:10]

        print("Computing Topic Diversity..")
        td = TopicDiversity(top_25_keywords)  # 25 is num of topic words
        scores['topic_diversity'].append(td.score(topk=25))

        print("Computing Inverted RBO..")
        rbo = InvertedRBO(top_10_keywords)
        scores['inverted_rbo'].append(rbo.score())

        # Coherence Word Embeddings
        # print("Computing Coherence Word Embeddings..")
        start = time.time()
        # we_coh = CoherenceWordEmbeddings(  # word2vec_path=word2vec_path,
        #     topics=top_10_keywords, binary=True)  # 10 is num of topic words
        # scores['coherence_we'].append(we_coh.score(topk=10))

        stop = time.time()
        time_elapsed = stop - start
        # print(f"Coherence Word Embeddings computed in {time_elapsed:.2f}s, {time_elapsed / 60:.2f} min")

        # Save the scores
        utils.save_model(self.work_folder, "scores", scores)

        return scores

    def get_topic_dict(self, n_keywords=10):
        """Create a topic dictionary and assign topic IDs to each topic.
        Used in `evaluate()`."""

        topic_dict = dict()
        for k in range(self.k):
            keywords = [word for word, _ in self.model.show_topic(k, topn=n_keywords)]
            topic_dict[k] = keywords

        return topic_dict

    # def visualize(self):
    #
    #     assigner = GensimAssigner(
    #         work_folder=None, model_path=None, dictionary_path=None,
    #         gensim_dataset_path=None, file_path=None,
    #      )
    #
    #     # set attributes that are required for pyLDAvis
    #     assigner.model = self.model,
    #     assigner.corpus_bow = self.corpus_bow,
    #     assigner.dictionary = self.dictionary,
    #
    #     return assigner.visualize()


# --------- Inference phase ---------

class GensimAssigner:

    def __init__(self, work_folder, model_path, dictionary_path,
                 gensim_dataset_path, file_path,
                 #doc_topic_dists=None, topic_term_dists=None,
                 ):
        self.work_folder = work_folder
        self.model = utils.load_model(work_folder, model_path)
        self.dictionary = utils.load_model(work_folder, dictionary_path)
        self.corpus_bow = utils.load_model(work_folder, gensim_dataset_path)
        self.file_path = file_path

        try:
            self.corpus_df = self._set_corpus()
            assert (self.corpus_df.shape[0] == len(self.corpus_bow))
        except Exception as e:
            print("The number of rows in CSV file does not match with that of Gensim (BoW) Dataset.", e)
            return

        self.k = len(self.model.get_topics())  # num of topics; for `get_topic_dict`
        self.word_freq_dict = None  # for WordCloud

        # Gensim LDA's topic assignment is deterministic; no need to import existing dists
        self.doc_topic_dists = None
        #self.topic_term_dists = None

    def _set_corpus(self):
        df = pd.read_csv(self.file_path, encoding='utf8', index_col='sent_id')
        # df['LDA_tokens'] = list(map(lambda x: len(str(x).split()), df['LDA_ready']))

        return df

    def get_topic_dict(self, n_keywords=10):
        """Create a topic dictionary and assign topic IDs to each topic."""

        topic_dict = dict()
        for k in range(self.k):
            keywords = [word for word, _ in self.model.show_topic(k, topn=n_keywords)]
            topic_dict[k] = keywords

        return topic_dict

    def assign_most_probable_topic(self):

        # fix doc-topic distribution to remove randomness in topic assignment
        if self.doc_topic_dists is None:
            doc_topic_dists = []
            for bow in self.corpus_bow:
                # model returns a list of tuples [(topic_id, prob), ()...]
                theta = [prob for _, prob in self.model[bow]]
                doc_topic_dists.append(theta)
            self.doc_topic_dists = doc_topic_dists

        # fix topic-term_dists to remove randomness in topic assignment
        #        if self.topic_term_dists is None:
        #            self.topic_term_dists = self.model.best_components

        # most probable topic ids
        topic_ids = np.argmax(self.doc_topic_dists, axis=1)
        probs = []
        for i, topic_id in enumerate(topic_ids):
            probs.append(self.doc_topic_dists[i][topic_id])

        # set results
        self.corpus_df['topic_id'] = topic_ids
        self.corpus_df['prob'] = probs

        # save doc_topic_dists & topic_term_dists
        #utils.save_model(self.work_folder, "doc_topic_dists", self.doc_topic_dists)
        #utils.save_model(self.work_folder, "topic_term_dists", self.topic_term_dists)

        return self.corpus_df

    def visualize(self):

        vis = pyLDAvis.gensim.prepare(topic_model=self.model,
                                      corpus=self.corpus_bow,
                                      dictionary=self.dictionary,
                                      sort_topics=False,
                                      )

        with open(path.join(self.work_folder, "pyLDAvis.html"), 'w') as f:
            pyLDAvis.save_html(vis, f)

        return vis

    def create_cloud(self, colormap='viridis', topic_id=0, top_n_keywords=100, figsize=(20, 15)):

        self.word_freq_dict = self._create_word_freq_dict(top_n_keywords)
        cloud = WordCloud(
            background_color='white',
            stopwords=None,
            random_state=0,
            colormap=colormap,
        ).generate_from_frequencies(self.word_freq_dict[topic_id])

        plt.figure(figsize=figsize)
        plt.imshow(cloud)
        plt.axis('off')
        plt.show()

        return cloud

    def _create_word_freq_dict(self, top_n_keywords):
        """Helper for create_wordcloud function.
        Returns a list of dictionaries."""

        topic_terms_all = []
        for i in range(self.k):
            topic_terms = dict()
            topic_term_dist = self.model.get_topic_terms(i, topn=top_n_keywords)
            for token_id, freq in topic_term_dist:
                topic_terms[self.dictionary[token_id]] = freq
            topic_terms_all.append(topic_terms)

        return topic_terms_all
